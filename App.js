import React, { Component } from 'react';
import { StatusBar, StyleSheet, View} from 'react-native';
import RoutedApp from './App/Routes/Router';

export default class App extends Component {
    render() {
        return(
            <View style={styles.container}>
                <StatusBar barStyle="light-content"/>
                <RoutedApp />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})