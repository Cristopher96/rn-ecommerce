import React from 'react';
import { StyleSheet, Image, Dimensions } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

const LoginGradient = () => {
    return (
        <LinearGradient colors={['#F85F62', '#F26C61', '#F27E67']} start={ { x: 0, y:0 } }  end={{ x: 1, y: 0 }} style={styles.header} >
            <Image source={require('../../assets/React-icon.png')}  style={{ height: 100, width: 100 }} />
        </LinearGradient>
    );
}

const styles = StyleSheet.create({
    header: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 300,
        width: Dimensions.get('window').width,
        position: 'absolute',
        top: 0,
    },
});
 
export default LoginGradient;