import React from 'react';
import { StyleSheet, ScrollView, SafeAreaView } from 'react-native';
import DrawerHeader from './DrawerHeader';

const CustomDrawer = () => {
    return (
        <ScrollView>
            <SafeAreaView 
                style={styles.container}
                forceInset={{ top: 'always', horizontal: 'never' }}
            >
                <DrawerHeader />
            </SafeAreaView>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
 
export default CustomDrawer;