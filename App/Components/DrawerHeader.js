import React from 'react';
import { StyleSheet, View } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

const DrawerHeader = () => {
    return (
        <LinearGradient colors={['#F27E67', '#F26C61', '#F85F62']} start={ { x: 0, y:0 } }  end={{ x: 1, y: 0 }} style={styles.header} >
            <View style={styles.headerContent}>

            </View>
        </LinearGradient>
    );
}

const styles = StyleSheet.create({
    header: {
        height: 88,
        width: '100%',
        position: 'absolute',
        top: 0,
        padding: 10,
        justifyContent: "center",
        alignItems: "center",
        borderBottomWidth: 1,
        borderBottomColor: '#F2F2F2'
    },
    headerContent: {
        backgroundColor: '#F2F2F2',
        borderRadius: 10,
        width: '100%',
        height: '100%',
        elevation: 3,
        shadowOffset: { width: 1, height: 1 },
        shadowColor: '#000',
        shadowOpacity: 0.5,
        shadowRadius: 2,

    }
});
 
export default DrawerHeader;