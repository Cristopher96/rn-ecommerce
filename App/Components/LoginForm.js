import React, { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

const LoginForm = ({ navigation }) => {

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [secure, setSecure] = useState(true);

    return (
        <View style={styles.formInput}>
            <Text style={styles.info} >USER NAME</Text>
            <TextInput 
                style={styles.inputs}
                value={username}
                onChangeText={ value => setUsername(value) }/>
            <Text style={styles.info} >PASSWORD</Text>
            <TextInput 
                style={styles.inputs}
                secureTextEntry={secure}
                value={password}
                onChangeText={ value => setPassword(value) }/>
            <TouchableOpacity 
                style={styles.button}
                onPress={ () => navigation.navigate('Home', {username}) }>
                <LinearGradient 
                    colors={['#F85F62', '#F26C61', '#F27E67']}
                    start={ { x: 0, y:0 } }  end={{ x: 1, y: 0 }}
                    style={styles.gradient}>
                    <Text style={styles.btnText} >Sign in</Text>
                </LinearGradient>
            </TouchableOpacity>
            <TouchableOpacity>
                <Text style={{ color: '#F27E67' }} >Create an account.</Text>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    formInput: {
        backgroundColor: '#FFFFFF',
        height: 270,
        width: 300,
        borderRadius: 10,
        marginTop: 130,
        alignItems: 'center',
        padding: 10,
        paddingTop: 15,
        elevation: 3,
        shadowOffset: { width: 1, height: 1 },
        shadowColor: '#000',
        shadowOpacity: 0.5,
        shadowRadius: 2,
    },
    inputs: {
        height: 40,
        width: '100%',
        padding: 5,
        backgroundColor: '#F2F2F2',
        borderRadius: 5,
        marginBottom: 20
    },
    info: {
        alignSelf: 'flex-start',
        fontSize: 10,
        fontWeight: 'bold',
        marginBottom: 5,
    },
    button: {
        width: '100%',
        height: 45,
        marginBottom: 25,
    },
    gradient: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
    },
    btnText: {
        fontSize: 16,
        color: '#FFFFFF',
    }
});
 
export default LoginForm;