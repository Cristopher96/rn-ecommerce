import React from 'react';
import { LinearGradient } from 'expo-linear-gradient';

const GradientHeader = () => {
    return (
        <LinearGradient colors={['#F85F62', '#F26C61', '#F27E67']} start={ { x: 0, y:0 } }  end={{ x: 1, y: 0 }} style={{flex: 1}} />
    );
}
 
export default GradientHeader;