import React from 'react';
import { createDrawerNavigator } from 'react-navigation-drawer';
import HomeStack from './HomeStack';
import CustomDrawer from '../Components/CustomDrawer';

const Drawer = createDrawerNavigator({
    Home: {
        screen: HomeStack
    }
}, {
    drawerPosition: "left",
    drawerType: "slide",
    hideStatusBar: true,
    contentComponent: () => (
        <CustomDrawer />
    )
})

export default Drawer;