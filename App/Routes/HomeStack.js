import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import Home from '../Screens/Home';
import { MaterialIcons } from '@expo/vector-icons';
import GradientHeader from '../Components/GradientHeader';

const screens = {
    Home: {
        screen: Home
    }
}

const HomeStack = createStackNavigator(screens, {
    defaultNavigationOptions: ({ navigation }) => {
        return {
            headerBackground: () => (
                <GradientHeader />
            ),
            headerLeft: () => (
                <MaterialIcons name='menu' size={30} color='#FFF' style={{ marginLeft: 20 }} onPress={ () => navigation.toggleDrawer() } />
            ),
            headerRight: () => (
                <MaterialIcons name='shopping-cart' size={30} color='#FFF' style={{ marginRight: 20 }} />
            ) ,
            title: ''
        }
    }
});

export default HomeStack;