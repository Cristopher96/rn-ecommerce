import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import Auth from '../Screens/Auth';
import AuthStack from './LoginStack';
import Drawer from './Drawer';

const RoutedApp = createAppContainer(
    createSwitchNavigator({
        AuthLoading: Auth,
        Auth: AuthStack,
        App: Drawer
    }, {
        initialRouteName: 'AuthLoading'
    })
);

export default RoutedApp;