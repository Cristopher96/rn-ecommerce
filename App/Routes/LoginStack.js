import { createStackNavigator } from 'react-navigation-stack';
import Login from '../Screens/Login';

const screens = {
    Login: {
        screen: Login,
        navigationOptions: {
            headerShown: false,
            gestureEnabled: false,
            headerBackTitle: null,
        }
    }
}

const AuthStack = createStackNavigator(screens);

export default AuthStack;