import React, {  Component } from 'react';
import { View, StyleSheet, ActivityIndicator, AsyncStorage } from 'react-native';

export default class Auth extends Component {

    componentDidMount = () => {
        this.checkSession();
    }

    checkSession = async () => {
        const userToken = await AsyncStorage.getItem('user');
        this.props.navigation.navigate( userToken ? 'Home' : 'Login' );
    }

    render() {
        return (
            <View style={styles.container}>
                <ActivityIndicator />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});