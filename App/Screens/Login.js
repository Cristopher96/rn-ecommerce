import React, { Component } from 'react';
import { StyleSheet, View, Keyboard, KeyboardAvoidingView, TouchableWithoutFeedback, Platform } from 'react-native';
import LoginForm from '../Components/LoginForm';
import LoginGradient from '../Components/LoginGradient';

export default class Login extends Component {
    render() {
        return (
            <KeyboardAvoidingView behavior={ Platform.OS == 'ios' ? 'padding' : 'height' } style={styles.container} >
                <TouchableWithoutFeedback onPress={ () => Keyboard.dismiss() } >
                    <View style={styles.container}>
                        <LoginGradient />
                        <LoginForm navigation={this.props.navigation} />
                    </View>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
});