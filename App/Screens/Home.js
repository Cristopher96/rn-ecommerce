import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

export default class Home extends Component {
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity
                    style={styles.btn}
                    onPress={ () => this.props.navigation.navigate('Login') } >
                    <LinearGradient
                        colors={['#F85F62', '#F26C61', '#F27E67']}
                        start={ { x: 0, y:0 } }  end={{ x: 1, y: 0 }}
                        style={styles.gradient}>
                        <Text style={styles.btnText} > Logout </Text>
                    </LinearGradient>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    btn: {
        width: 300,
        height: 45,
    },
    btnText: {
        fontSize: 16,
        color: '#FFF'
    },
    gradient: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
    },
});